
## Process Excel Bansos

Process bansos excel using AWS python 3

## quickstart local development

The `.env` original is from Sapawarga-app
```sh
cp src/.env.example src/.env # modify this file
docker-compose build
docker-compose up -d
docker-compose exec app yarn
```
## Credits

Ganjar Setia M <ganjar.setia@gmail.com>
