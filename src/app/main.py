__author__ = "Ganjar Setia"
__email__ = "ganjar.setia@gmail.com"
__maintainer__ = "Ganjar Setia"

from fastapi import FastAPI, Request, HTTPException
from fastapi.exception_handlers import (
    http_exception_handler,
    request_validation_exception_handler,
)
from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from pydantic import BaseModel

import os
import json
import boto3
import botocore
import pandas as pd
import numpy as np
import sys
import pymysql
import io
import json
from datetime import datetime
import pytz
import logging
from environs import Env
import subprocess
import xlsxwriter

app = FastAPI()
env = Env()
env.read_env()  # read .env file, if it exists

app_env_dev = env.int("YII_ENV_DEV", 0)
rds_host  = env('MYSQL_HOST', 'localhost')
db_port = int(env('MYSQL_PORT', '3306'))
db_username = env('MYSQL_USER', 'root')
db_password = env('MYSQL_PASSWORD', 'root')
db_name = env('MYSQL_DATABASE', 'sapawarga_db_development')

rds_host_dtks  = env('MYSQL_HOST_DTKS', 'localhost')
db_username_dtks = env('MYSQL_USER_DTKS', 'root')
db_password_dtks = env('MYSQL_PASSWORD_DTKS', 'root')
db_name_dtks = env('MYSQL_DATABASE_DTKS', 'dtks')
db_port_dtks = env.int('MYSQL_PORT_DTKS', 3306)

s3_bucket = env('APP_STORAGE_S3_BUCKET', 'sapawarga-staging')
s3_folder_invalid = env('S3_FOLDER_INVALID', 'bansos-bnba-invalid')
s3_folder_invalid_verval = env('S3_FOLDER_INVALID_VERVAL', 'bansos-verval-invalid')
keep_invalid_template_format = env.bool('KEEP_INVALID_TEMPLATE_FORMAT', False)
logger = logging.getLogger()

# dev only
if app_env_dev == 1:
    rds_host_dtks  = rds_host
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

col_invalid = 'status_validasi'
main_sheet = 'Sheet1'
tz = pytz.timezone('Asia/Jakarta')
jakarta_now = datetime.now(tz)
item_process = 0
item_valid = 0
total_row = 0
err_msg = ''
temp_folder = '/tmp/'

s3 = boto3.client(
    's3',
    env('APP_STORAGE_S3_BUCKET_REGION', 'ap-southeast-1'),
    aws_access_key_id = env('APP_STORAGE_S3_KEY', None),
    aws_secret_access_key = env('APP_STORAGE_S3_SECRET', None)
)

class ExcelRequest(BaseModel):
    bucket_name: str
    path_file_s3: str
    file_name: str
    s3_records: str = None


# buka kode dibawah ini untuk standar error
"""
@app.exception_handler(StarletteHTTPException)
async def custom_http_exception_handler(request, exc):
    print(f"OMG! An HTTP error!: {exc}")
    return await http_exception_handler(request, exc)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    print(f"OMG! The client sent invalid data!: {exc}")
    return await request_validation_exception_handler(request, exc)
"""


@app.on_event("startup")
async def startup_event():
    try:
        conn = connect_db_sapawarga()
        conn_dtks = connect_db_dtks()
        logger.info('Success Connect 2 DB')
        conn.close()
        conn_dtks.close()
    except pymysql.MySQLError as e:
        logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
        logger.error(e)
        sys.exit()


@app.post("/process-excel/")
def process_bansos(request: ExcelRequest):
    try:
        bucket_name = request.bucket_name
        path_file_s3 = request.path_file_s3
        file_name = request.file_name
        full_path_file_name_ori = temp_folder + file_name
        type_bansos = os.path.split(path_file_s3)
        type_bansos = type_bansos[0]
        status_kode = 0
        sheet_exist = True
        if type_bansos == 'bansos-bnba':
            type_bansos = 'BNBA'
        elif type_bansos == 'bansos-verval':
            type_bansos = 'verval'

        logger.info('Downloading %s from S3... bucket: %s' % (path_file_s3, bucket_name))
        s3.download_file(bucket_name, path_file_s3, full_path_file_name_ori)
        xls = pd.ExcelFile(full_path_file_name_ori)
        sheets = xls.sheet_names
        if main_sheet not in sheets:
            logger.info(sheets)
            sheet_exist = False
            df = pd.read_excel(full_path_file_name_ori)
        else:
            df = pd.read_excel(full_path_file_name_ori, sheet_name=main_sheet)

        logger.info(df)
        notes = 'Impor dimulai'
        set_status_process(type_bansos, file_name, status_kode, notes, '')

        column = list(df.columns)
        if type_bansos == 'verval':
            required_column = ['nik', 'nomor_kk', 'nama', 'kode_kabkota', 'kode_kec', 'kode_kel', 'rw', 'rt', 'alamat', 'nomor_hp', 'jumlah_tanggungan', 'id_lapangan_usaha', 'id_status_pekerjaan', 'penghasilan_sebelum_covid19', 'penghasilan_sesudah_covid19', 'contoh']
        else:
            required_column = ['nik', 'nama_krt', 'kode_kab', 'kode_kec', 'kode_kel', 'rw', 'rt', 'alamat']

        if (not set(column) > set(required_column)) or sheet_exist == False:
            logger.error("ERROR: Kolom tidak valid atau Sheet1 ga ada, di file %s" % file_name)
            notes = 'File tidak sesuai template'
            status_kode = 21
            set_status_process(type_bansos, file_name, status_kode, notes, '')
            global err_msg
            err_msg = "Kolom tidak valid"
            raise HTTPException(status_code=422, detail=err_msg)

        list_bps = None
        user_id = -1
        verval_type = None
        id_bansos_type = None
        is_dtks = None
        start_row = 1

        # get current tahap
        sql = ("SELECT current_tahap_bnba, current_tahap_verval FROM `beneficiaries_current_tahap`")
        conn = connect_db_sapawarga()
        current_tahap = get_one(conn, sql)
        current_tahap_bnba = int(current_tahap[0])
        current_tahap_verval = int(current_tahap[1])

        if type_bansos == 'verval':
            sql = ("SELECT kabkota_code, user_id, verval_type FROM `bansos_verval_upload_histories` WHERE `file_path` = '{}'").format(path_file_s3)
        else:
            sql = ("SELECT kabkota_code, user_id, bansos_type FROM `bansos_bnba_upload_histories` WHERE `file_path` = '{}'").format(path_file_s3)

        conn = connect_db_sapawarga()
        upload_history = get_one(conn, sql)
        if upload_history:
            kabkota_code = upload_history[0]
            user_id = upload_history[1]
            verval_type = upload_history[2] if type_bansos == 'verval' else verval_type
            id_bansos_type = int(upload_history[2]) if type_bansos == 'BNBA' else None
            is_dtks = 1 if id_bansos_type and id_bansos_type > 10 else None
            sql = ("SELECT code_bps FROM `areas` WHERE `code_bps` LIKE '{}%'").format(kabkota_code)
            conn = connect_db_sapawarga()
            list_bps = get_all(conn, sql)
            list_bps = [row[0] for row in list_bps]

        # global total_row
        if type_bansos == 'verval':
            total_row = get_total_rows_verval(df)
            # TODO: find dynamic row start. Currently verval start from 3rd row
            start_row = 2
        else:
            total_row = len(df)

        logger.info("Total rows: {0}".format(total_row))

        df['nik'] = pd.to_numeric(df['nik'], downcast='integer', errors='coerce')
        if type_bansos == 'verval':
            df['nomor_kk'] = pd.to_numeric(df['nomor_kk'], downcast='integer', errors='coerce')
        else:
            df['no_kk'] = pd.to_numeric(df['no_kk'], downcast='integer', errors='coerce')

        # make nomor_hp null for DB
        df['nomor_hp'].fillna('', inplace=True)
        df.drop_duplicates(subset ="nik", keep = 'first', inplace = True)
        df[col_invalid] = df.apply(process_all, args=(type_bansos, list_bps, user_id, verval_type, current_tahap_bnba, current_tahap_verval, id_bansos_type, is_dtks), axis=1)
        print(df[['nik', col_invalid]])

        # global item_valid
        item_valid = len(df.query('status_validasi == True'))
        item_invalid = len(df.query('status_validasi != True'))
        item_invalid = item_invalid - len(df.query('status_validasi == "dihapus"'))
        df_invalid = df.query('status_validasi != True').copy()
        df_invalid.query('status_validasi != "dihapus"', inplace=True)
        if not df_invalid.empty:
            df_invalid['nik'] = remove_decimal(df_invalid['nik'])
            if type_bansos == 'verval':
                df_invalid['nomor_kk'] = remove_decimal(df_invalid['nomor_kk'])
            else:
                df_invalid['no_kk'] = remove_decimal(df_invalid['no_kk'])

            logger.info('write to excel...')
            file_name_invalid = 'invalid_' + file_name
            full_path_file_name_invalid = temp_folder + file_name_invalid
            writer = pd.ExcelWriter(full_path_file_name_invalid, engine='xlsxwriter')
            df_invalid.to_excel(writer, sheet_name=main_sheet, index=False)
            writer.save()

            if keep_invalid_template_format:
                col_position = getColumnLetter(df_invalid, col_invalid)
                logger.info('Run CMD: node src/app/modify-excel.js {} {} {} {} {} {} {} {}'.format(col_position, main_sheet, col_invalid, start_row + 1, total_row, type_bansos, full_path_file_name_ori, full_path_file_name_invalid))
                MyOut = subprocess.Popen(['node', 'src/app/modify-excel.js', col_position, main_sheet, col_invalid, str(start_row + 1), str(total_row), type_bansos, full_path_file_name_ori, full_path_file_name_invalid],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
                stdout, stderr = MyOut.communicate()

                output_modify = stdout.decode('utf-8')
                output_modify = output_modify.strip()
                logger.info(output_modify)

                if stderr == None and output_modify == 'modify success':
                    logger.info('Success keep template file ' + full_path_file_name_invalid)
                else:
                    logger.error(stderr)
                    notes = 'Error proses nodejs membuat file invalid'
                    status_kode = -1
                    set_status_process(type_bansos, file_name, status_kode, notes, '')

            # upload it to S3
            logger.info('Uploading invalid file ' + full_path_file_name_invalid)
            path_folder_invalid = s3_folder_invalid_verval if type_bansos == 'verval' else s3_folder_invalid
            logger.info(path_folder_invalid + '/' + file_name_invalid)
            s3.upload_file(full_path_file_name_invalid, s3_bucket, path_folder_invalid + '/' + file_name_invalid)
            logger.info('Delete %s and %s' % (full_path_file_name_ori, full_path_file_name_invalid))
            os.remove(full_path_file_name_invalid)
            os.remove(full_path_file_name_ori)

            # update the status
            # old message
            # notes = '%s dari %s data gagal diupload' % (item_invalid, total_row)
            percent_valid = (item_valid / total_row) * 100
            status_kode = 20
            notes = '{0} ({1:.2f}%) diupload, {2} gagal diupload. Total data {3}'.format(item_valid, percent_valid, item_invalid, total_row)
            set_status_process(type_bansos, file_name, status_kode, notes, path_folder_invalid + '/' + file_name_invalid)

        else:
            notes = 'Sukses'
            status_kode = 10
            set_status_process(type_bansos, file_name, status_kode, notes, '')

        logger.info('Total ' + str(item_valid) + ' NIK Processed. ' + notes + ' Tipe ' + type_bansos + ' ' + file_name + ' ')
        return {
            'statusCode': 200,
            'body': json.dumps('Total ' + str(item_valid) + ' NIK Processed')
        }
    except Exception as e:
        notes = 'Error proses'
        if status_kode < 20:
            set_status_process(type_bansos, file_name, -1, notes, '')
        logger.error('%s %s' % (notes, path_file_s3))
        logger.error(str(e))
        raise HTTPException(status_code=422, detail=err_msg)


def process_all(row, type_bansos, list_bps, user_id, verval_type, current_tahap_bnba, current_tahap_verval, id_bansos_type, is_dtks):
    if type_bansos == 'verval' and row['contoh'] == 'contoh':
        logger.info(str(row['nama']) + ' di skip')
        return 'dihapus'

    if type_bansos == 'verval' and pd.isna(row['nik']) and pd.isna(row['nama_kab']):
        return 'dihapus'

    res = True
    invalid_messages = []
    # global item_process

    allowedPrefixNik = [
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '21',
        '31', '32', '33', '34', '35', '36', '51', '52', '53', '61',
        '62', '63', '64', '65', '71', '72', '73', '74', '75', '76',
        '81', '82', '91', '92'
    ]

    if type_bansos == 'verval':
        label_nama = 'nama'
        label_kabkota = 'kode_kabkota'
        label_no_kk = 'nomor_kk'
        row['jumlah_tanggungan'] = row['jumlah_tanggungan'] if pd.notna(row['jumlah_tanggungan']) else 0
        row['nomor_hp'] = row['nomor_hp'] if pd.notna(row['nomor_hp']) else None
        row['id_lapangan_usaha'] = row['id_lapangan_usaha'] if pd.notna(row['id_lapangan_usaha']) else None
        row['id_status_pekerjaan'] = row['id_status_pekerjaan'] if pd.notna(row['id_status_pekerjaan']) else None
        row['penghasilan_sebelum_covid19'] = row['penghasilan_sebelum_covid19'] if pd.notna(row['penghasilan_sebelum_covid19']) else 0
        row['penghasilan_sesudah_covid19'] = row['penghasilan_sesudah_covid19'] if pd.notna(row['penghasilan_sesudah_covid19']) else 0
    else:
        label_nama = 'nama_krt'
        label_kabkota = 'kode_kab'
        label_no_kk = 'no_kk'

    nik = intToStr(row['nik']) if pd.notna(row['nik']) else np.nan
    no_kk = intToStr(row[label_no_kk]) if pd.notna(row[label_no_kk]) else np.nan
    row[label_kabkota] = int(row[label_kabkota]) if (pd.notna(row[label_kabkota]) and not hasAlpha(str(row[label_kabkota]))) else np.nan
    row['kode_kec'] = int(row['kode_kec']) if (pd.notna(row['kode_kec']) and not hasAlpha(str(row[label_kabkota]))) else np.nan
    row['kode_kel'] = int(row['kode_kel']) if (pd.notna(row['kode_kel']) and not hasAlpha(str(row[label_kabkota]))) else np.nan

    try:
        if ((pd.isna(nik)) or ((len(nik) == 0))):
            invalid_messages.append('NIK Kosong')

        if (pd.notna(nik) and (len(nik) != 16)):
            invalid_messages.append('NIK harus 16 digit')

        if (pd.notna(nik) and nik[-4:] == '0000'):
            invalid_messages.append('4 angka akhir NIK tidak boleh 0000')

        if (pd.notna(nik) and (nik[:2] not in allowedPrefixNik)):
            invalid_messages.append('2 angka awal NIK bukan kode valid')

        if type_bansos == 'verval' and pd.isna(row['nomor_kk']):
            invalid_messages.append('Nomor KK kosong')

        if pd.isna(row[label_nama]):
            invalid_messages.append('Nama kosong')

        if (pd.notna(row[label_nama]) and (len(row[label_nama]) < 3 )):
            invalid_messages.append('Nama kurang dari 2 huruf')

        if (pd.notna(row[label_nama]) and (row[label_nama].isdigit())):
            invalid_messages.append('Nama tidak boleh angka semua')

        if (pd.notna(row[label_nama]) and not check_alphanum(row[label_nama])):
            invalid_messages.append('Nama hanya boleh mengandung huruf dan angka')

        if (pd.isna(row['rw']) or row['rw'] == 0):
            invalid_messages.append('RW kosong atau 0')

        if (pd.isna(row['rt']) or row['rt'] == 0):
            invalid_messages.append('RT kosong atau 0')

        if (pd.notna(row['rt']) and (not check_alphanum(str(row['rt'])) or not hasNumbers(str(row['rt'])))):
            invalid_messages.append('RT hanya boleh mengandung huruf/angka dan minimal 1 angka')

        if (pd.notna(row['rw']) and (not check_alphanum(str(row['rw'])) or not hasNumbers(str(row['rw'])))):
            invalid_messages.append('RW hanya boleh mengandung huruf/angka dan minimal 1 angka')

        if pd.isna(row['alamat']):
            invalid_messages.append('Alamat kosong')

        if (pd.notna(row['alamat']) and (len(row['alamat']) < 11 )):
            invalid_messages.append('Alamat kurang dari 11 huruf')

        if type_bansos == 'verval' and pd.isna(row['nomor_hp']):
            invalid_messages.append('nomor_hp kosong')

        if pd.isna(row[label_kabkota]):
            invalid_messages.append(label_kabkota + ' kosong')

        if pd.isna(row['kode_kec']):
            invalid_messages.append('kode_kec kosong')

        if pd.isna(row['kode_kel']):
            invalid_messages.append('kode_kel kosong')

        if list_bps != None:
            if (pd.notna(row[label_kabkota]) and (not str(row[label_kabkota]).isdigit())):
                invalid_messages.append(label_kabkota + ' harus angka')
            elif not str(row[label_kabkota]) in list_bps:
                invalid_messages.append(label_kabkota + ' tidak terdaftar')

            if (pd.notna(row['kode_kec']) and (not str(row['kode_kec']).isdigit())):
                invalid_messages.append('kode_kec harus angka')
            elif not str(row['kode_kec']) in list_bps:
                invalid_messages.append('kode_kec tidak terdaftar')

            if (pd.notna(row['kode_kel']) and (not str(row['kode_kel']).isdigit())):
                invalid_messages.append('kode_kel harus angka')
            elif not str(row['kode_kel']) in list_bps:
                invalid_messages.append('kode_kel tidak terdaftar')

        if invalid_messages:
            res = ', '.join(invalid_messages)
            res = str(row.name) + '|' + res
            logger.info(res)
        else:
            if type_bansos == 'verval':
                switcher = {
                    "kabkota": 9,
                    "kecamatan": 7,
                    "kelurahan": 5,
                }
                stat_verif = switcher.get(verval_type, 0)
                conn2 = connect_db_sapawarga()
                with conn2.cursor() as cur:
                    # input verval
                    sql = ("SELECT * FROM `beneficiaries` WHERE `nik` = '{}'").format(nik)
                    conn = connect_db_sapawarga()
                    cek_exist = get_one(conn, sql)
                    if cek_exist:
                        sql = "UPDATE `beneficiaries` SET `domicile_address` = %s, `address` = %s, `domicile_rt` = %s, `domicile_rw` = %s, `updated_by` = %s, `updated_at` = %s, `status_verification` = %s, `tahap_3_verval` = %s, `is_import_verval` = %s WHERE `id` = %s"
                        logger.info(sql % (row['alamat'], row['alamat'], row['rt'], row['rw'], user_id, int(jakarta_now.timestamp()), stat_verif, stat_verif, 1, cek_exist[0]))
                        cur.execute(sql, (row['alamat'], row['alamat'], row['rt'], row['rw'], user_id, int(jakarta_now.timestamp()), stat_verif, stat_verif, 1, cek_exist[0]))
                        logger.info("updated %s %s" % (nik, row['nama']))
                    else:
                        sql = "INSERT INTO `beneficiaries` (`nik`, `no_kk`, `name`, `domicile_province_bps_id`, `domicile_kabkota_bps_id`, `domicile_kec_bps_id`, `domicile_kel_bps_id`, `domicile_rt`, `domicile_rw`, `domicile_address`, `phone`, `total_family_members`, `job_type_id`, `job_status_id`, `income_before`, `income_after`, `status_verification`, `tahap_3_verval`, `is_import_verval`, `status`, `created_by`, `created_at`, `updated_at`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                        logger.info(sql % (nik, no_kk, row['nama'], 32, row['kode_kabkota'], row['kode_kec'], row['kode_kel'], row['rt'], row['rw'], row['alamat'], row['nomor_hp'], row['jumlah_tanggungan'], row['id_lapangan_usaha'], row['id_status_pekerjaan'], row['penghasilan_sebelum_covid19'], row['penghasilan_sesudah_covid19'], stat_verif, stat_verif, 1, 10, user_id, int(jakarta_now.timestamp()), int(jakarta_now.timestamp())))
                        cur.execute(sql, (nik, no_kk, row['nama'], 32, row['kode_kabkota'], row['kode_kec'], row['kode_kel'], row['rt'], row['rw'], row['alamat'], row['nomor_hp'], row['jumlah_tanggungan'], row['id_lapangan_usaha'], row['id_status_pekerjaan'], row['penghasilan_sebelum_covid19'], row['penghasilan_sesudah_covid19'], stat_verif, stat_verif, 1, 10, user_id, int(jakarta_now.timestamp()), int(jakarta_now.timestamp())))
                        logger.info("Inserted %s %s" % (nik, row['nama']))

                conn2.commit()
                conn2.close()
            else:
                # input BNBA
                conn2 = connect_db_sapawarga()
                with conn2.cursor() as cur:
                    # item_process += 1
                    # logger.info('Process row %s' % (item_process))
                    sql = ("SELECT idartbdt, nik FROM `arts` WHERE `nik` = '{}'").format(row['nik'])
                    conn_dtks = connect_db_dtks()
                    cek_dtks = get_one(conn_dtks, sql)

                    sql = ("SELECT * FROM `beneficiaries` WHERE `nik` = '{}' AND `status_verification` = 2").format(nik)
                    conn = connect_db_sapawarga()
                    cek_status_verification = get_one(conn, sql)

                    if cek_dtks or cek_status_verification:
                        logger.info(row)
                        logger.info('ditolak DTKS/verif, data sudah ada')
                        # data yg ditolak dihapus saja
                        invalid_messages.append('dihapus')
                        res = ', '.join(invalid_messages)
                        if cek_status_verification:
                            sql = ("UPDATE `beneficiaries` SET `domicile_rt` = '{}', `domicile_rw` = '{}', `domicile_address` = '{}' WHERE `beneficiaries`.`id` = {}").format(int(row['rt']), int(row['rw']), str(row['alamat']), cek_status_verification[0])
                            conn = connect_db_sapawarga()
                            execute_query(conn, sql)
                            logger.info("updated beneficiaries denied %s %s" % (nik, row['nama_krt']))
                    else:
                        sql = ("SELECT * FROM `beneficiaries_bnba_tahap_1` WHERE `nik` = '{}' and `tahap_bantuan` = {}").format(nik, current_tahap_bnba)
                        conn = connect_db_sapawarga()
                        cek_exist_bnba1 = get_one(conn, sql)
                        if cek_exist_bnba1:
                            sql = "UPDATE `beneficiaries_bnba_tahap_1` SET `nama_krt` = %s, `kode_kab` = %s, `kode_kec` = %s, `kode_kel` = %s, `rw` = %s, `rt` = %s, `alamat` = %s WHERE `id` = %s"
                            cur.execute(sql, (row['nama_krt'], row['kode_kab'], row['kode_kec'], row['kode_kel'], row['rw'], row['rt'], row['alamat'], cek_exist_bnba1[0]))
                            logger.info("updated %s %s" % (nik, row['nama_krt']))
                        else:
                            sql = "INSERT INTO `beneficiaries_bnba_tahap_1` (`is_dtks`, `id_tipe_bansos`, `nik`, `nama_krt`, `kode_kab`, `kode_kec`, `kode_kel`, `rw`, `rt`, `alamat`, `tahap_bantuan`, `nama_kab`, `nama_kec`, `nama_kel`, `no_kk`, `jumlah_art_tanggungan`, `nomor_hp`, `lapangan_usaha`, `status_kedudukan`, `penghasilan_sebelum_covid19`, `penghasilan_setelah_covid`, `keterangan`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            cur.execute(sql, (is_dtks, id_bansos_type, row['nik'], row['nama_krt'], row['kode_kab'], row['kode_kec'], row['kode_kel'], row['rw'], row['rt'], row['alamat'], current_tahap_bnba, row['nama_kab'], row['nama_kec'], row['nama_kel'], no_kk, row['jumlah_art_tanggungan'], row['nomor_hp'], row['lapangan_usaha'], row['status_kedudukan'], row['penghasilan_sebelum_covid19'], row['penghasilan_setelah_covid'], row['keterangan']))
                            logger.info("inserted %s %s " % (nik, row['nama_krt']))

                conn2.commit()
                conn2.close()

        return res

    except ValueError:
        return 'gagal cek'


def connect_db_sapawarga():
    return pymysql.connect(rds_host, user=db_username, passwd=db_password, db=db_name, port=db_port, connect_timeout=5)


def connect_db_dtks():
    return pymysql.connect(rds_host_dtks, user=db_username_dtks, passwd=db_password_dtks, db=db_name_dtks, port=db_port_dtks, connect_timeout=5)


def execute_query(conn, sql):
    logger.info(sql)
    with conn.cursor() as cur:
        cur.execute(sql)

    conn.commit()
    conn.close()
    return True


def get_one(conn, sql):
    logger.info(sql)
    with conn.cursor() as cur:
        cur.execute(sql)
        res = cur.fetchone()
        conn.close()
        return res


def get_all(conn, sql):
    logger.info(sql)
    with conn.cursor() as cur:
        cur.execute(sql)
        res = cur.fetchall()
        conn.close()
        return res


def set_status_process(type_bansos, file_name, status, notes, invalid_file_path):
    logger.info('%s - %s %s' % (str(status), notes, type_bansos))

    if type_bansos == 'verval':
        sql = ("SELECT * FROM `bansos_verval_upload_histories` WHERE `file_path` LIKE '%{}'").format(file_name)
    else: # default to BNBA
        sql = ("SELECT * FROM `bansos_bnba_upload_histories` WHERE `file_path` LIKE '%{}'").format(file_name)
    conn = connect_db_sapawarga()
    upload_history = get_one(conn, sql)

    if upload_history:
        id_upload_history = upload_history[0]
        if type_bansos == 'verval':
            sql = ("UPDATE `bansos_verval_upload_histories` SET `status` = {}, `notes` = '{}', `invalid_file_path` = '{}', `updated_at` = {} WHERE `id` = {}").format(status, notes, invalid_file_path, int(jakarta_now.timestamp()),  id_upload_history)
        else:
            sql = ("UPDATE `bansos_bnba_upload_histories` SET `status` = {}, `notes` = '{}', `invalid_file_path` = '{}', `updated_at` = {} WHERE `id` = {}").format(status, notes, invalid_file_path, int(jakarta_now.timestamp()),  id_upload_history)
        conn = connect_db_sapawarga()
        execute_query(conn, sql)
    else:
        logger.error('File tidak ada di tabel upload_histories')


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def hasAlpha(inputString):
    return any(char.isalpha() for char in inputString)


def check_alphanum(inputString):
    isalnum = inputString.replace(' ', '')
    isalnum = isalnum.replace('.', '')
    isalnum = isalnum.replace(',', '')
    isalnum = isalnum.replace("'", '')
    isalnum = isalnum.isalnum()
    return isalnum


def remove_decimal(input_df):
    input_df = input_df.astype('int', errors='ignore')
    return input_df.astype('str', errors='ignore')


def intToStr(input):
    output = int(input)
    return str(output)


def getColumnLetter(df, col_name):
    cols = list(df.columns)
    index = cols.index(col_name)
    return xlsxwriter.utility.xl_col_to_name(index)


def get_total_rows_verval(df):
    total = 0
    for i in range(1, len(df.index)) :
        total_null_in_row = df.iloc[i].isnull().sum()
        if total_null_in_row < len(df.columns):
            total += 1

    return total
