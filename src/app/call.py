import requests
import json

API_ENDPOINT = "http://172.17.0.1:5053/process-excel/"


def main():
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json',
    }

    # data to be sent to api
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-bnba/3273080_1_20200429_205846.xlsx", "file_name":"3273080_1_20200429_205846.xlsx", "s3_records":"dummy"}'
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-bnba/3273_51_20200610_141923.xlsx", "file_name":"3273_51_20200610_141923.xlsx", "s3_records":"dummy"}'
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-bnba/3277_52_20200609_154625.xlsx", "file_name":"3277_52_20200609_154625.xlsx", "s3_records":"dummy"}'
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-bnba/3277_52_20200609_151527.xlsx", "file_name":"3277_52_20200609_151527.xlsx", "s3_records":"dummy"}' # sample BNBA ada di DB
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-verval/3273_kabkota_20200630_152016.xlsx", "file_name":"3273_kabkota_20200630_152016.xlsx", "s3_records":"dummy"}' # sample VERVAL ada di DB
    data = '{"bucket_name":"sapawarga-staging", "path_file_s3":"bansos-verval/Test_Import_Manual_140620.xlsx", "file_name":"Test_Import_Manual_140620.xlsx", "s3_records":"dummy"}' # sample VERVAL ada di DB

    try:
        requests.post(API_ENDPOINT, headers=headers, data=data, timeout=0.0000000001)
    except requests.exceptions.ReadTimeout: 
        pass

    return {
        'statusCode': 200,
        'body': json.dumps(data)
    }

if __name__ == "__main__":
    main()
