/**
 * @author Ganjar Setia <ganjar.setia@gmail.com>
 */

const Excel = require('exceljs');

const main = async (args) => {
  try {
    // uncomment below for dev test
    // example bnba dev
    // args = ['V', 'Sheet1', 'status_invalid', 1, 5, 'bnba', '/tmp/3277_52_20200609_151527.xlsx', '/tmp/invalid_3277_52_20200609_151527.xlsx']
    
    // example verval dev
    // args = ['X', 'Sheet1', 'status_invalid', 3, 20, 'verval', '/tmp/Test_Import_Manual_140620.xlsx', '/tmp/invalid_Test_Import_Manual_140620.xlsx']

    let [colName, sheetName, invalidColName, startRow, totalRow, type_bansos, pathOriFileName, pathInvalidFileName] = args
    startRow = parseInt(startRow)
    totalRow = parseInt(totalRow)

    let workbook = new Excel.Workbook()
    await workbook.xlsx.readFile(pathInvalidFileName)

    const worksheet = workbook.getWorksheet(sheetName)
    const statInvalid = worksheet.getColumn(colName).values
    statInvalid.splice(0, 2) // remove 2 element from beginning. Its empty & column title

    const invalidData = statInvalid.map(row => {
      const invalidCell = row.split('|')
      
      // add + 2. ExcelJS row start from 1 & its column. For starting row data is from 2
      parseInvalid = {'rowNum': parseInt(invalidCell[0]) + 2}
      parseInvalid[invalidColName] = invalidCell[1]
      return parseInvalid
    })
    
    await workbook.xlsx.readFile(pathOriFileName)
    const worksheetOri = workbook.getWorksheet(sheetName)
    worksheetOri.getCell(colName + '1').value = invalidColName
    // fill status_invalid
    const newInvalidRow = []
    invalidData.map(row => {
      worksheetOri.getCell(colName + row.rowNum).value = row[invalidColName]
      newInvalidRow.push(worksheetOri.getRow(row.rowNum).values)
    })

    if (type_bansos === 'bnba') {
      const newInvalidCol = worksheetOri.getColumn(colName)
      newInvalidCol.eachCell(function(cell, rowNumber) {
          if (rowNumber !== 1) { // skip header
              if (!cell.value) worksheetOri.spliceRows(rowNumber, 1)
          }
      })
    } else { // verval
      worksheetOri.insertRows(startRow, newInvalidRow) // insert invalid row from first row
      worksheetOri.spliceRows(newInvalidRow.length + startRow, totalRow) // remove row with old content
    }

    // replace original invalid file
    await workbook.xlsx.writeFile(pathInvalidFileName)

    console.log('modify success')
  } catch (error) {
    console.log(error)
    console.log('modify gagal')
  }
}

const args = process.argv.slice(2);
main(args)
